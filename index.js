/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';

import './src/config/Colors';
import './src/config/Assets';
import './src/config/Spacings';
import './src/config/Typo';

import {name as appName} from './app.json';
import App from './App';
AppRegistry.registerComponent(appName, () => App);
