import React from 'react';
import {StyleSheet, Animated} from 'react-native';
import {Colors, Image, Text, View} from 'react-native-ui-lib';
import {useAniTransX} from '../../../hooks/useAniTransX';
import {IExerciseItem} from '../../../types/IWorkout';

const NextExercise = ({exerciseNext}: {exerciseNext: IExerciseItem}) => {
  const [stylesAni] = useAniTransX();

  return (
    <Animated.View style={stylesAni}>
      <Text m16 margin-16 color={Colors.white}>
        Next
      </Text>
      <View row marginH-16>
        <Image
          source={{uri: exerciseNext.exercise.img}}
          style={{
            width: 60,
            height: 60,
          }}
        />
        <View marginL-16>
          <Text h16 color={Colors.white}>
            {exerciseNext.exercise.name}
          </Text>
          <Text
            b10
            color={
              Colors.white
            }>{`${exerciseNext.time} seconds rest: ${exerciseNext.rest}`}</Text>
        </View>
      </View>
    </Animated.View>
  );
};

export default NextExercise;

const styles = StyleSheet.create({});
