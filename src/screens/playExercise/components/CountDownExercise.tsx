import React from 'react';
import {StyleSheet, Animated} from 'react-native';
import {View, Text, Colors} from 'react-native-ui-lib';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';
import {useAniTransX} from '../../../hooks/useAniTransX';

interface Props {
  duration: number;
  onComplete: () => void;
  status: 'Duration' | 'Rest';
}

const CountDownExercise = ({duration, onComplete, status}: Props) => {
  const [isPlaying, setIsPlaying] = React.useState(false);
  const [stylesAni] = useAniTransX(() => {
    setIsPlaying(true);
  });

  return (
    <Animated.View style={stylesAni}>
      <Text m17 center color={Colors.white} marginV-24 marginB-8>
        {status}
      </Text>
      <CountdownCircleTimer
        isPlaying={isPlaying}
        duration={duration}
        size={60}
        strokeWidth={4}
        colors={[
          ['#004777', 0.4],
          ['#F7B801', 0.4],
          ['#A30000', 0.2],
        ]}
        onComplete={(totalElapsedTime: number) => {
          setIsPlaying(false);
          onComplete();
        }}>
        {({remainingTime, animatedColor}) => (
          <>
            <Animated.Text style={{color: animatedColor, fontSize: 24}}>
              {remainingTime}
            </Animated.Text>
          </>
        )}
      </CountdownCircleTimer>
    </Animated.View>
  );
};

export default CountDownExercise;

const styles = StyleSheet.create({});
