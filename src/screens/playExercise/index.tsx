import {RouteProp, useRoute} from '@react-navigation/core';
import React from 'react';
import {StyleSheet} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {Button, Colors, Image, Text, View} from 'react-native-ui-lib';
import {RootStackParamList} from '../../nav/RootStack';
import {IExerciseItem} from '../../types/IWorkout';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import ItemPlayExercise from './components/ItemPlayExercise';
import CountDownExercise from './components/CountDownExercise';
import NextExercise from './components/NextExercise';

const PlayExercise = () => {
  const route = useRoute<RouteProp<RootStackParamList, 'PlayExercise'>>();
  const exercise = route.params.exercises;

  const [currentIndex, setCurrentIndex] = React.useState(0);

  const [status, setStatus] = React.useState<'Duration' | 'Rest' | 'Finish'>(
    'Duration',
  );

  const exerciseNext = React.useMemo(() => {
    if (currentIndex === exercise.length - 1) return null;
    return exercise[currentIndex + 1];
  }, [currentIndex, exercise]);

  const exerciseCurrent = React.useMemo(() => {
    return exercise[currentIndex];
  }, [currentIndex, exercise]);

  const isExerciseLast = React.useMemo(() => {
    return currentIndex === exercise.length - 1;
  }, [currentIndex, exercise]);

  const renderItem = React.useCallback(
    ({item, index}: {item: IExerciseItem; index: number}) => {
      return <ItemPlayExercise item={item} play={index === currentIndex} />;
    },
    [currentIndex],
  );
  const refFlatlist = React.useRef<FlatList>(null);

  return (
    <View flex backgroundColor={Colors.dark10}>
      <View>
        <FlatList
          data={exercise}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
          horizontal
          pagingEnabled
          scrollEnabled={false}
          ref={refFlatlist}
        />
      </View>
      <View centerH marginV-16>
        {status === 'Duration' ? (
          <CountDownDuration
            time={exerciseCurrent.time}
            onComplete={() => {
              setStatus('Rest');
            }}
          />
        ) : (
          <CountDownRest
            time={exerciseCurrent.rest}
            onComplete={() => {
              if (!isExerciseLast) {
                setCurrentIndex(prev => {
                  if (prev === exercise.length - 1) return prev;
                  return prev + 1;
                });

                setStatus('Duration');
                refFlatlist.current?.scrollToIndex({index: currentIndex + 1});
              } else {
                setStatus('Finish');
              }
            }}    
          />
        )}
      </View>
      {!!exerciseNext && <NextExercise exerciseNext={exerciseCurrent} />}
      <View flex />
      <View
        marginH-16
        style={{
          marginBottom: getBottomSpace() || 16,
        }}>
        <Button label="Next Exercise" />
      </View>
    </View>
  );
};

export default PlayExercise;

const styles = StyleSheet.create({});

const CountDownDuration = ({
  time,
  onComplete,
}: {
  time: number;
  onComplete: () => void;
}) => {
  return (
    <CountDownExercise
      duration={time}
      onComplete={onComplete}
      status={'Duration'}
    />
  );
};

const CountDownRest = ({
  time,
  onComplete,
}: {
  time: number;
  onComplete: () => void;
}) => {
  return (
    <CountDownExercise
      duration={time}
      onComplete={onComplete}
      status={'Rest'}
    />
  );
};
