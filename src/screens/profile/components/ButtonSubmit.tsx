import {NavigationProp, useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Button, View} from 'react-native';
import {RootStackParamList} from '../../../nav/RootStack';

interface Props {
  userInfo: {
    name: string;
    age: string;
  };
}

const ButtonSubmit = ({userInfo}: Props) => {
  const navigation = useNavigation<NavigationProp<RootStackParamList>>();
  return (
    <Button
      title="Cập nhật thông tin"
      onPress={() => {
        navigation.navigate('UpdateProfile', {
          name: userInfo.name,
          age: userInfo.age,
        });
      }}
    />
  );
};

export default ButtonSubmit;

const styles = StyleSheet.create({});
