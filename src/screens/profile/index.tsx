import React from 'react';
import {StyleSheet, View, Button, Switch} from 'react-native';
import Container from '../../components/Container';
import Txt from '../../components/Txt';
import useBoolean from '../../hooks/useBoolean';
import ButtonSubmit from './components/ButtonSubmit';
import {useSelector} from 'react-redux';
import {RootState} from '../../reduxs/store';
import {IUserState} from '../../reduxs/userSlice';
import ButtonResetUserInfo from './components/ButtonResetUserInfo';

const Profile = React.memo(() => {
  const userInfo = useSelector<RootState, IUserState>(state => state.userInfo);
  const [valueSwitch, setValueSwitch] = React.useState<boolean>(false);
  const [showModal, onShowModal, onHideModal] = useBoolean();
  return (
    <Container>
      <Txt>{`Ten2: ${userInfo.name}`}</Txt>
      <Txt>{`Age: ${userInfo.age}`}</Txt>

      <ButtonSubmit userInfo={{name: userInfo.name, age: userInfo.age}} />
      <ButtonResetUserInfo />
      <Switch value={valueSwitch} onValueChange={setValueSwitch} />

      <Button title="Show modal" onPress={onShowModal} />
      {!!showModal && (
        <View style={{width: 100, height: 100, backgroundColor: 'red'}}>
          <Button title="Hide modal" onPress={onHideModal} />
        </View>
      )}
    </Container>
  );
});

export default Profile;

const styles = StyleSheet.create({});
