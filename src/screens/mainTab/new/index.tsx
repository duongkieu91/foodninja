import React from 'react';
import {StyleSheet, Dimensions, FlatList, ScrollView} from 'react-native';
import {useSelector} from 'react-redux';
import {RootState} from '../../../reduxs/store';
import {View, Image} from 'react-native-ui-lib';
import Banner from './components/Banner';
import ListHorizontal from './components/ListHorizontal';
import ItemWorkout from './components/ItemWorkout';
import ListWorkout from './components/ListWorkout';

const widthScreen = Dimensions.get('window').width;
const widthImg = widthScreen;
const heightImg = (widthImg / 375) * 256;

const New = () => {
  const nameUser = useSelector<RootState, string>(
    state => state.auth.customer.name,
  );

  return (
    <View flex bg-white>
      <Image
        assetGroup="imgNewScreen"
        assetName="ic_bg"
        width={widthImg}
        height={heightImg}
        style={{position: 'absolute'}}
      />
      <ListWorkout
        ListHeaderComponent={
          <>
            <Banner />
            <ListHorizontal />
          </>
        }
      />
    </View>
  );
};

export default New;

const styles = StyleSheet.create({
  loopCarousel: {
    position: 'absolute',
    bottom: 15,
    left: 10,
  },
});
