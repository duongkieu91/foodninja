import {RouteProp, useRoute} from '@react-navigation/core';
import React from 'react';
import {Dimensions, ScrollView, StyleSheet, Animated} from 'react-native';
import {Assets, Colors, Text, View, Image} from 'react-native-ui-lib';
import {RootStackParamList} from '../../nav/RootStack';
import * as Icon from 'react-native-iconly';
import ItemExercise from './components/ItemExercise';
import dayjs from 'dayjs';
import HeaderDetailWorkout from './components/HeaderDetailWorkout';
import {Avatar} from 'react-native-ui-lib';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {getDayTime} from '../../utils/handleString';
import ButtonStart from './components/ButtonStart';

const widthBanner = Dimensions.get('window').width;
const heightBanner = (widthBanner / 1600) * 1000;
const imgBackgrounds = [
  Assets.imgWorkout.imgWorkout1,
  Assets.imgWorkout.imgWorkout2,
  Assets.imgWorkout.imgWorkout3,
  Assets.imgWorkout.imgWorkout4,
  Assets.imgWorkout.imgWorkout5,
  Assets.imgWorkout.imgWorkout6,
  Assets.imgWorkout.imgWorkout7,
  Assets.imgWorkout.imgWorkout8,
  Assets.imgWorkout.imgWorkout9,
  Assets.imgWorkout.imgWorkout10,
];

const DetailWorkout = () => {
  const route = useRoute<RouteProp<RootStackParamList, 'DetailWorkout'>>();
  const workout = route.params.item;
  const scrollY = React.useRef(new Animated.Value(0)).current;
  return (
    <View style={{flex: 1, backgroundColor: '#FFF'}}>
      <HeaderDetailWorkout scrollY={scrollY} title={workout.name} />

      <Animated.ScrollView
        onScroll={Animated.event(
          [
            {
              nativeEvent: {
                contentOffset: {
                  y: scrollY,
                },
              },
            },
          ],
          // {
          //   useNativeDriver: true,
          // },
        )}
        contentContainerStyle={{
          paddingBottom: getBottomSpace(),
        }}>
        <View
          style={{
            height: heightBanner,
            width: widthBanner,
            backgroundColor: 'red',
          }}>
          <Image
            source={imgBackgrounds[workout.muscle_part_id % 10]}
            //   source={imgBackgrounds[1]}
            // assetGroup="imgWorkout"
            // assetName="imgWorkout1"
            style={{
              height: heightBanner,
              width: widthBanner,
            }}
            overlayType={Image.overlayTypes.BOTTOM}
          />
        </View>

        <Text h28 marginH-16 marginV-12>
          {workout.name}
        </Text>
        <View row centerV marginH-16 paddingV-4>
          <Icon.Chat color={Colors.grey10} />
          <Text marginL-12>Hoạt động: {workout.commentCount} comment</Text>
        </View>
        <View row centerV marginH-16 paddingV-4>
          <Icon.Calendar color={Colors.grey10} />
          <Text marginL-12>
            Ngày tạo: {dayjs(workout.created_at).format('MM/DD/YYYY')}
          </Text>
        </View>
        <View row centerV marginH-16 paddingV-4 marginB-12>
          <Icon.Work color={Colors.grey10} />
          <Text marginL-12>
            Calo tiêu thụ: {parseInt(workout.calo, 10)} calos
          </Text>
        </View>
        <Text h17 marginH-16 marginV-4>
          Danh sách bài tập ({workout.exercise_items.length})
        </Text>
        {workout.exercise_items.map((item, index) => (
          <ItemExercise item={item.exercise} key={item.id} index={index} />
        ))}
        <Text h17 marginH-16 marginV-4>
          Danh sách bình luận ({workout.commentCount})
        </Text>
        {workout.comments.map(item => {
          return (
            <View
              row
              marginH-16
              marginV-8
              style={{
                borderBottomWidth: 1,
                borderColor: Colors.grey60,
              }}
              key={item.id}>
              <Avatar size={40} />
              <View marginL-12 flex>
                <Text m16>customer_id: {item.customer_id}</Text>
                <Text m8 color={Colors.grey30}>
                  {getDayTime(item.created_at)}
                </Text>
                <Text m15 marginV-8 flex>
                  {item.content}
                </Text>
              </View>
            </View>
          );
        })}
      </Animated.ScrollView>
      <ButtonStart
        delayNumber={workout.exercise_items.length}
        exercises={workout.exercise_items}
      />
    </View>
  );
};

export default DetailWorkout;

const styles = StyleSheet.create({});
