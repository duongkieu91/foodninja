import React from 'react';
import {StyleSheet, TouchableOpacity, Animated, Easing} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {View, Text, Colors} from 'react-native-ui-lib';
import * as Icon from 'react-native-iconly';
import {useNavigation} from '@react-navigation/core';

interface Props {
  scrollY: Animated.Value;
  title: string;
}

const HeaderDetailWorkout = ({scrollY, title}: Props) => {
  const {goBack} = useNavigation();
  const opacity = scrollY.interpolate({
    inputRange: [0, 120],
    outputRange: [0, 1],
    easing: Easing.linear,
  });
  return (
    <View
      style={{
        paddingTop: getStatusBarHeight(true),
        paddingBottom: 12,
        flexDirection: 'row',
        position: 'absolute',
        zIndex: 1,
        // backgroundColor: 'red',
        width: '100%',
      }}>
      <TouchableOpacity
        style={{
          marginLeft: 12,
        }}
        onPress={goBack}>
        <Icon.ArrowLeft size={32} color="#000" />
      </TouchableOpacity>
      <Animated.View
        style={{
          backgroundColor: '#FFF',
          flex: 1,
          ...StyleSheet.absoluteFillObject,
          zIndex: -1,
          opacity: opacity,
          borderBottomWidth: 1,
          borderBottomColor: Colors.grey60,
          paddingTop: getStatusBarHeight(true),
          paddingBottom: 12,
          justifyContent: 'center',
        }}>
        <Text h17 center>
          {title}
        </Text>
      </Animated.View>
    </View>
  );
};

export default HeaderDetailWorkout;

const styles = StyleSheet.create({});
