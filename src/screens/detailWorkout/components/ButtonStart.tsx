import {NavigationProp, useNavigation} from '@react-navigation/core';
import React from 'react';
import {StyleSheet, TouchableOpacity, Animated, Dimensions} from 'react-native';
import {Colors, Text, View} from 'react-native-ui-lib';
import {RootStackParamList} from '../../../nav/RootStack';
import {IExerciseItem} from '../../../types/IWorkout';

const widthScreen = Dimensions.get('window').width;
const ButtonAni = Animated.createAnimatedComponent(TouchableOpacity);

interface Props {
  delayNumber: number;
  exercises: IExerciseItem[];
}

const ButtonStart = ({delayNumber, exercises}: Props) => {
  const {navigate} = useNavigation<NavigationProp<RootStackParamList>>();
  const scale = React.useRef(new Animated.Value(1)).current;
  const transX = React.useRef(new Animated.Value(widthScreen)).current;
  const opacity = React.useRef(new Animated.Value(0)).current;

  React.useLayoutEffect(() => {
    Animated.timing(transX, {
      toValue: 0,
      duration: 350,
      useNativeDriver: true,
      delay: delayNumber * 350,
    }).start();
    Animated.timing(opacity, {
      toValue: 1,
      duration: 350,
      useNativeDriver: true,
      delay: delayNumber * 350,
    }).start();
  }, []);
  const onPressIn = React.useCallback(() => {
    Animated.timing(scale, {
      toValue: 0.95,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }, []);
  const onPressOut = React.useCallback(() => {
    Animated.timing(scale, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();

    navigate('PlayExercise', {
      exercises: exercises,
    });
  }, []);

  return (
    <ButtonAni
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      activeOpacity={0.7}
      style={{
        transform: [{scale: scale}, {translateX: transX}],
        opacity: opacity,
        position: 'absolute',
        bottom: 50,
        width: '40%',
        height: 48,
        backgroundColor: Colors.primary,
        alignSelf: 'center',
        borderRadius: 99,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1,
      }}>
      <Text h17 color="#FFF">
        Start
      </Text>
    </ButtonAni>
  );
};

export default ButtonStart;

const styles = StyleSheet.create({});
