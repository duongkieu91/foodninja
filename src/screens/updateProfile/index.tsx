import React from 'react';
import {StyleSheet, View, TextInput, Button} from 'react-native';
import Container from '../../components/Container';
import Txt from '../../components/Txt';
import useBoolean from '../../hooks/useBoolean';
import {useSelector} from 'react-redux';
import {RootState} from '../../reduxs/store';
import {IUserState} from '../../reduxs/userSlice';
import ButtonSubmit from './ButtonSubmit';
import ButtonUpdateTheme from './ButtonUpdateTheme';

const UpdateProfile = () => {
  const userInfo = useSelector<RootState, IUserState>(state => state.userInfo);
  const [updateUserInfo, setUpdateUserInfo] = React.useState(userInfo);
  const [showModal, onShowModal, onHideModal] = useBoolean();
  return (
    <Container>
      <View>
        <Txt>Tên</Txt>
        <TextInput
          placeholder="Họ và tên"
          defaultValue={updateUserInfo.name}
          onChangeText={(name: string) => {
            setUpdateUserInfo({...updateUserInfo, name});
          }}
        />
      </View>
      <View>
        <Txt>Tuổi</Txt>
        <TextInput
          placeholder="Nhập số tuổi"
          defaultValue={updateUserInfo.age}
          onChangeText={(age: string) => {
            setUpdateUserInfo({...updateUserInfo, age});
          }}
        />
      </View>

      <View>
        <Txt>Địa chỉ</Txt>
        <TextInput
          placeholder="Nhập địa chỉ"
          defaultValue={updateUserInfo.address}
          onChangeText={(address: string) => {
            setUpdateUserInfo({...updateUserInfo, address});
          }}
        />
      </View>

      <ButtonSubmit updateUserInfo={updateUserInfo} />
      <ButtonUpdateTheme />

      <Button title="Show modal" onPress={onShowModal} />
      {!!showModal && (
        <View style={{width: 100, height: 100, backgroundColor: 'red'}}>
          <Button title="Hide modal" onPress={onHideModal} />
        </View>
      )}
    </Container>
  );
};

export default UpdateProfile;

const styles = StyleSheet.create({});
