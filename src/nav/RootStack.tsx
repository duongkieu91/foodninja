import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Onboarding from '../screens/onboarding';
import Login from '../screens/auth/login';
import MainTab from './MainTab';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../reduxs/store';
import {
  EStatusAuth,
  getAuthAsync,
  IAuth,
  onLogin,
  updateStatusAuth,
} from '../reduxs/authSlice';
import {View, Colors} from 'react-native-ui-lib';
import {ActivityIndicator, Alert} from 'react-native';
import URL from '../config/Api';
import DetailWorkout from '../screens/detailWorkout';
import {IExerciseItem, IWorkout} from '../types/IWorkout';
import PlayExercise from '../screens/playExercise';

export type RootStackParamList = {
  ProWelcomefile: undefined;
  OnBoarding: undefined;
  CreateAccount: undefined;
  SignUp: undefined;
  MainTab: undefined;
  Setting: undefined;
  CategoryDetail: undefined;
  CategoryDetailSub: undefined;
  TopicDetail: undefined;
  ProfileAndFriend: undefined;
  ProfileCouponsVouchers: undefined;
  MyTopic: undefined;
  Search: undefined;
  FAQ: undefined;
  Filter: undefined;
  Login: undefined;
  DetailWorkout: {item: IWorkout};
  PlayExercise: {exercises: IExerciseItem[]};
};

const Stack = createNativeStackNavigator();
const RootStack = () => {
  const statusAuth = useSelector<RootState, EStatusAuth>(
    state => state.auth.statusAuth,
  );
  const dispatch = useDispatch();
  const checkLogin = React.useCallback(async () => {
    const auth: IAuth | null = await getAuthAsync();
    if (auth) {
      //call api validate auth
      fetch(URL.ValidateToken, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: auth.token,
        }),
      })
        .then(response => response.json())
        .then((json: {success: boolean; message: string}) => {
          const success = json.success;
          //token fail
          if (!success) {
            Alert.alert('Thông báo', json.message);
            dispatch(updateStatusAuth({statusAuth: EStatusAuth.unauth}));
            return;
          }
          //token success
          dispatch(onLogin(auth));
          return json;
        });
    } else {
      dispatch(updateStatusAuth({statusAuth: EStatusAuth.unauth}));
    }
  }, []);
  React.useEffect(() => {
    checkLogin();
  }, []);

  if (statusAuth === EStatusAuth.check) {
    return (
      <View flex center>
        <ActivityIndicator color={Colors.primary} />
      </View>
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        {statusAuth === EStatusAuth.unauth ? (
          <>
            <Stack.Screen
              name="Onboarding"
              component={Onboarding}
              options={{headerShown: false}}
            />
            <Stack.Screen name="Login" component={Login} />
          </>
        ) : (
          <>
            <Stack.Screen
              name="MainTab"
              component={MainTab}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="DetailWorkout"
              component={DetailWorkout}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="PlayExercise"
              component={PlayExercise}
              options={{
                headerStyle: {
                  backgroundColor: Colors.dark10,
                },
                headerTintColor: Colors.white,
                title: '',
                headerBackTitleVisible: false,
              }}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootStack;
